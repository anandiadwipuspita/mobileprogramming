import React, {Component} from 'react';
import { StyleSheet, Text, View, StatusBar, Image } from 'react-native';

import Logo from './Logo';
import Form from './form';
export default class login extends Component {
    render() {
        return(
            <View style = {StyleSheet.container}>
                <Logo />
                <Form />
                <View style = {styles.signupTextCont}>
                <Text style={styles.signupText}> Don't have an account yet? </Text>
                <Text style={styles.signupButton}> Sign Up </Text> 
                </View>
            </View>
        )
    }
}
const styles = StyleSheet.create({
    container: {
          flex: 1,
          backgroundColor: '#5D8B8B',
          alignItems: 'center',
          justifyContent: 'center'
        },
    signupTextCont: {
            flexGrow: 1,
            alignItems: 'flex-end',
            justifyContent: 'center',
            marginVertical: 16,
            flexDirection: 'row'
        },
    signupText: {
        color: ' #D06E3F',
        fontSize: 16
    },
    signupButton: {
        color: '#DEB266',
        fontSize: 16,
        fontWeight: '500'
    }
});