import React, {Component} from 'react';
import { StyleSheet, Text, View, Image, Button } from 'react-native';

export default class Biodata extends Component{
    render(){
        return(
            <View style = {styles.container}>
                <View style={{marginTop:64, alignItems:'center'}}>
                    <View style = {styles.avatarcontainer}>
                        <Image style={styles.avatar} source = {require('./assets/Adin.jpg')} />
                    </View>
                    <Text style={styles.name}>Annandia Dwi Puspita</Text>
                </View>
                <View style={styles.statsContainer}>
                    <View style={styles.stat}>
                        <Text style={styles.statAmount}>04</Text>
                        <Text style={styles.statTitle}>Post</Text>
                    </View>
                    <View style={styles.stat}>
                        <Text style={styles.statAmount}>94</Text>
                        <Text style={styles.statTitle}>Followers</Text>
                    </View>
                    <View style={styles.stat}>
                        <Text style={styles.statAmount}>61</Text>
                        <Text style={styles.statTitle}>Following</Text>
                    </View>
                </View>
                <View style={styles.status}>
                    <Text style={styles.study}>Program studi : Teknik Informatika </Text>
                </View>
                <View style={styles.class}>
                    <Text style={styles.kelas}>Kelas : Pagi A </Text>
                </View>
                <View style={styles.instagram}>
                    <Text style={styles.ig}>Instagram : @anandiadin </Text>
                   
                </View>
                <View style={styles.facebook}>
                    <Text style={styles.fb}>Facebook : Anandia Dwi P</Text>
                </View>
            </View>
        )
    }
} 
const styles = StyleSheet.create({
    container:{
        backgroundColor: '#809E8C',
        flex:1,
        width: 400
    },
    avatarcontainer: {
        shadowColor:' #5D8B8B',
        shadowRadius: 15,
        shadowOpacity: 0.4
    },
    avatar: {
        width: 136,
        height: 136,
        borderRadius: 68
    },
    name: {
        color: '#DEB266',
        marginTop: 24,
        fontSize: 20,
        fontWeight: 'bold'
    },
    statsContainer: {
        flexDirection: "row",
        justifyContent: 'space-between',
        margin: 50,
        bottom: 20
    },
    stat: {
        alignItems: 'center',
        flex : 1
    },
    statAmount:{
        color:'#D06E3F',
        fontSize: 18,
        fontWeight: 'bold'
    },
    statTitle: {
        fontSize: 16,
        color: '#DEB266',
        fontWeight: '300',
        marginTop: 4
    },
    status: {
        bottom: 20,
        left: 60
    },
    study:{
        justifyContent: 'flex-end',
        color: '#DEB266',
        fontSize: 20,
        fontWeight: 'bold',
    },
    class: {
        bottom:20,
        left: 60
    },
    kelas:{
        fontWeight: 'bold',
        color: '#DEB266',
        fontSize : 20
    },
    instagram: {
        bottom : 20,
        left : 60
    },
    ig: {
        fontSize: 20,
        color: '#DEB266',
        fontWeight: 'bold'
    },
    facebook:{
        bottom: 20,
        left : 50
    },
    fb: {
        fontWeight :'bold',
        color: '#DEB266',
        fontSize: 20
    },
    state : {
        fontSize : 20,
        left : 60,
        bottom :-15
    }
});