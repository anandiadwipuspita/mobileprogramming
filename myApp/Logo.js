import React, {Component} from 'react';
import { StyleSheet, Text, View, Image } from 'react-native';

export default class Logo extends Component {
    render(){
        return(
            <View style = {styles.container}>
                    <Image styles = {{width:10, height: 10}}
                    source = {require('./assets/Allahuakbar.jpg')}/>
                    <Text style={styles.logoText}>Welcome to EXOPLANET </Text>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
          flexGrow: 10,
          justifyContent: 'flex-end',
          alignItems: 'center'
        }, 
    logoText: {
        marginVertical: 5,
        fontSize: 25,
        color: '#5D8B8B'
    }
});