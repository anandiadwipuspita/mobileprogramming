import React, {Component} from 'react';
import { StyleSheet, Text, View, TextInput, Image, TouchableOpacity } from 'react-native';

export default class Logo extends Component {
    render(){
        return(
            <View style = {styles.container}>
                <TextInput style={styles.inputBox} underlineColorAndroid='#A35752'
                 placeholder='Username/Email' 
                 placeholderTextColor=' #DE9250'
                 />
                 <TextInput style={styles.inputBox} underlineColorAndroid='#A35752'
                 placeholder='Password'
                 secureTextEntry={true} 
                 placeholderTextColor=' #DE9250'
                 />
                 <TouchableOpacity style={styles.button}>
                     <Text style={styles.buttonText}>Login</Text>
                 </TouchableOpacity>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
          flexGrow: 1,
          justifyContent: 'center',
          alignItems: 'center'
        },
    inputBox:{
        width:300,
        height:50,
        backgroundColor: '#809E8C',
        borderRadius: 25,
        paddingHorizontal:15,
        fontSize: 16,
        color:'#fff',
        marginVertical: 9
    },
    button:{
        width: 150,
        height:50,
        backgroundColor: '#5D8B8B',
        borderRadius:25,
        marginVertical:10,
        paddingVertical:13
    },
    buttonText: {
        fontSize:17,
        fontWeight:'500',
        color:'#DEB266',
        textAlign:'center'
    },
});
