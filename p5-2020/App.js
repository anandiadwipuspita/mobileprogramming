import React, { Component } from 'react';
import { View, StyleSheet } from 'react-native';
import ExView from './exView';
import ExText from './exText';
import ExImage from './exImage';
import ExScroll from './exScroll'; 
import ExTouch from './exTouch';
import ExTextInput from './exTextInput';
import Layout from './file-layout/Layout';
import Prop from './Props';
import ExFlatList from './exFlatList';

export default class App extends Component {
  render() {
    return (
      <View>
        <ExTextInput />
      </View>
    )
  }
};
