//loop-while
console.log('Looping Pertama')
var loop = 2;
while(loop < 20){
    if(loop %2==0){
        console.log(loop + '-I LOVE CODING');
    } 
    loop++
}
console.log('\n');
console.log('Looping Kedua')
while(loop >= 2){
    if(loop %2==0){
        console.log(loop + '- I will become fullstack javascript developer');
    }
    loop--
}

console.log ('\n');

//loop-for
for(var s=1;s<=20;s++){
    if(s%2==0){
        console.log (s+ "- Informatika")
    } else {
        console.log(s+ "- Teknik")
        console.log(s+ "- I LOVE CODING")
    }
}

//Membuat Persegi Panjang #
var z='  ';

for(var p = 0; p < 4; p++){
    for(var q = 0; q <8; q++){
        z+='#';
    }
    z+='\n';
}
console.log(z);

//Membuat Tangga
var r = '  ';
for(t = 1; t<= 7; t++){
    for (u = 1; u<=t; u++){
        r+= '#';
    }
    r +='\n';
}
console.log (r);

//Membuat Papan Catur
for(var angka = 1; angka <=4; angka++){
    console.log('# # # # #');
    console.log(' # # # # #');
}
