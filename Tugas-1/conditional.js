//if-else 
var nama = "John"
var peran = ""
if(nama==""){
    console.log ("Nama Harus diisi!")
} else if(nama=="John" || peran==""){
    console.log ("Hallo John, Pilih peranmu untuk memulai game!")
} else if(nama=="Jane" || peran=="Penyihir"){
    console.log ("Selamat datang di Dunia WereWolf, Jane")
    console.log ("Halo Penyihir Jane, kamu dapat melihat siapa yang menjadi WereWolf")
} else if(nama=="Jenita" || peran=="Guard"){
    console.log ("Selamat datang di Dunia WereWolf,Jenita")
    console.log ("Halo Guard Jenita, kamu akan membantu melindungi temanmu dari serangan WereWolf")
} else if(nama=="Junaedi" || peran=="WereWolf"){
    console.log ("Selamat datang di Dunia WereWolf, Junaedi")
    console.log ("Halo WereWolf Junaedi, kamu akan memakan mangsa setiap malam!")
} else {
    console.log ("Game Berakhir") 
} 

//switch case
var hari = 12;
var bulan = 04;
var tahun = 1994;
if(hari <=31 || bulan<=12 || tahun<=1900 || tahun>=2200){
    switch (bulan){
        case 1 :
            bulan = 'Januari'
            break;
        case 2 :
            bulan = 'Februari'
            break;
        case 3 :
            bulan = 'Maret'
            break;
        case 4 :
            bulan = 'April'
            break;
        case 5 :
            bulan = 'Mei'
            break;
        case 6 : 
            bulan = 'Juni'
            break;
        case 7 :
            bulan = 'Juli'
            break;
        case 8 :
            bulan = 'Agustus'
            break;
        case 9 :
            bulan = 'September'
            break;
        case 10 :
            bulan = 'Oktober'
            break;
        case 11 :
            bulan = 'November'
            break;
        case 12 :
            bulan = 'Desember'
            break;
    default:
    bulan = ('Tidak Ada Bulan')
    }
    console.log (hari+' '+bulan+' '+tahun)
}